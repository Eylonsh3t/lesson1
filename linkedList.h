#pragma once

typedef struct stack
{
	int element;
	struct stack* next;
} stack;

void insertToStack(stack** head, stack* newNode);
void deleteNodeFromStack(stack** head);
stack* createNodeToInsert();
void printStack(stack* head);