#pragma once
#include <iostream>
#include "linkedList.h"
using namespace std;

stack* createNodeToInsert()
{
	stack* newNode = new stack;
	
	newNode->next = NULL;
	do
	{
		cout << "\nInsert positive number you want to add: ";
		cin >> newNode->element;
	} while (newNode->element < 0);

	return newNode;
}

void insertToStack(stack** head, stack* newNode)
{
	stack* curr = *head;
	stack* next = NULL;
	stack* tempNode;

	if (!*head) // empty list!
	{
		*head = newNode;
	}
	else
	{
		next = curr;
		tempNode = newNode;
		*head = newNode;
		curr = *head;
		curr->next = next;
	}
}

void deleteNodeFromStack(stack** head)
{
	*head = (*head)->next;
}

void printStack(stack* head)
{
	stack* curr = head;
	cout << "\nThe stack from the last to enter to the first to enter:\n";
	while (curr) // when curr == NULL, that is the end of the list, and loop will end (NULL is false)
	{
		cout << curr->element;
		cout << "\n";
		curr = curr->next;
	}
	cout << "\n";
}