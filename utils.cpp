#pragma once
#include <iostream>
#include <bits.h>
#include "utils.h"
using namespace std;

#define ARRAY_SIZE 6
#define DYNAMIC_ARRAY_SIZE 10

int main()
{
	int arr[] = { 1, 2, 3, 4, 5, 6 };
	int* dArr = NULL;
	unsigned int size = ARRAY_SIZE;
	cout << "The array:\n";
	printArray(arr, size);
	reverse(arr, int(size) - 1);
	cout << "\nThe reversed array:\n";
	printArray(arr, size);
	dArr = reverse10();
	cout << "\nThe reversed array:\n";
	printArray(dArr, DYNAMIC_ARRAY_SIZE);

	delete[] dArr;
	return 0;
}

void reverse(int* nums, unsigned int size)
{
	//Code from: https://www.geeksforgeeks.org/write-a-program-to-reverse-an-array-or-string/
	int start = 0;
	while (start < int(size))
	{
		int temp = nums[start];
		nums[start] = nums[size];
		nums[size] = temp;
		start++;
		size--;
	}
}

void printArray(int* nums, unsigned int size)
{
	for (int i = 0; i < int(size); i++)
	{
		cout << nums[i] << "	";
	}
}

int* reverse10()
{
	int i = 0;
	int start = 0;
	int size = DYNAMIC_ARRAY_SIZE - 1;
	int* nums = new int[DYNAMIC_ARRAY_SIZE];
	cout << "\n";
	for (i = 0; i < DYNAMIC_ARRAY_SIZE; i++)
	{
		cout << "\nEnter number: ";
		cin >> nums[i];
	}

	while (start < size)
	{
		int temp = nums[start];
		nums[start] = nums[size];
		nums[size] = temp;
		start++;
		size--;
	}
	return nums;
}