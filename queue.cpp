#pragma once
#include <iostream>
#include "Queue.h"
using namespace std;

queue* createQueue()
{
	queue* q = new queue;
	queueSize(q);
	initQueue(q, q->size);
	q->numbersInQueue = 0;
	q->newValue = 0;
	q->isEmpty = QUEUE_EMPTY;
	return q;
}

void printMenu()
{
	cout << "\n1 - Enter new number into the queue.\n2 - Remove the first number in the queue.\n3 - Print the queue.\n4 - Exit the program.\n";
}

int userChoice()
{
	int choice = 0;
	do 
	{
		cout << "Type your choice: ";
		cin >> choice;
	} while (choice < 0); //While choice is negative
	return choice;
}

void queueSize(queue* q)
{
	do
	{
		cout << "Type the size of the queue: ";
		cin >> q->size;
	} while (q->size > 100); //While size is negative
}

void initQueue(queue* q, unsigned int size)
{
	q->queueArray = new int[size + 1]; //When i will remove the first number in the queue i will move all the numbers one location forward so i need one location with null in it.
}

void cleanQueue(queue* q)
{
		delete[] q->queueArray;
}

void newValue(queue* q)
{
	do
	{
		cout << "Enter number to add the queue: ";
		cin >> q->newValue;
	} while (q->newValue > 2000000); //While the new value is negative
}

void enqueue(queue* q, unsigned int newValue)
{
	if (q->numbersInQueue != q->size)
	{
		q->queueArray[q->numbersInQueue] = q->newValue;
		q->numbersInQueue++;
		q->isEmpty = 0;
	}
	else
	{
		cout << "The queue is full";
	}
}

int dequeue(queue* q)
{
	int i = 0;
	if (q->isEmpty != QUEUE_EMPTY)
	{
		cout << "Removing the first number from the queue.\n";
		while (i < q->numbersInQueue)
		{
			q->queueArray[i] = q->queueArray[i + 1];
			i++;
		}
		q->numbersInQueue--;
		cout << "Done!";
	}
	else
	{
		cout << "Can't remove the first number in the queue if the queue is empty.";
	}
	if (q->numbersInQueue == 0)
	{
		q->isEmpty = QUEUE_EMPTY;
	}

	return q->isEmpty;
}

void printQueue(queue* q)
{
	if (q->isEmpty != QUEUE_EMPTY)
	{
		cout << "This is the queue from Last to first:\n";
		for (int i = q->numbersInQueue; i > 0; i--)
		{
			cout << q->queueArray[i - 1];
			cout << "	";
		}
			
	}
	else
	{
		cout << "The queue is empty.";
	}
}